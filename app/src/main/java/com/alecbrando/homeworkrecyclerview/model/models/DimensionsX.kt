package com.alecbrando.homeworkrecyclerview.model.models

data class DimensionsX(
    val large: LargeX,
    val medium: Medium,
    val small: SmallX,
    val tiny: TinyX
)