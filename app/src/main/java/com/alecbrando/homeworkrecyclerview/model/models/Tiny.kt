package com.alecbrando.homeworkrecyclerview.model.models

data class Tiny(
    val height: Int,
    val width: Int
)