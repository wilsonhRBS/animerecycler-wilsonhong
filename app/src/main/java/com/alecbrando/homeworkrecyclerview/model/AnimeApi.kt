package com.alecbrando.homeworkrecyclerview.model

import com.alecbrando.homeworkrecyclerview.model.models.Animes
import com.alecbrando.homeworkrecyclerview.model.models.Data

interface AnimeApi {
    fun getAnimeList(animes: Animes): List<Data>
}