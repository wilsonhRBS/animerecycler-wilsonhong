package com.alecbrando.homeworkrecyclerview.model

import com.alecbrando.homeworkrecyclerview.delay_time
import com.alecbrando.homeworkrecyclerview.model.models.Animes
import com.alecbrando.homeworkrecyclerview.model.models.Data
import com.alecbrando.homeworkrecyclerview.util.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.withContext

object AnimeRepo {
    private val animeApi: AnimeApi = object: AnimeApi {
        override fun getAnimeList(animes: Animes): List<Data> {
            return animes.data
        }
    }

    suspend fun fetchAnimeList(animes: Animes): Resource = withContext(Dispatchers.IO) {
        delay(delay_time)
        return@withContext Resource.Success(animeApi.getAnimeList(animes))
    }
}