package com.alecbrando.homeworkrecyclerview.util

import com.alecbrando.homeworkrecyclerview.model.models.Data

sealed class Resource {
    data class Success(val data: List<Data>): Resource()
    object Loading: Resource()
}

