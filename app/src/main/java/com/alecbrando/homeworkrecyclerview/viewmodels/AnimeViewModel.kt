package com.alecbrando.homeworkrecyclerview.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.alecbrando.homeworkrecyclerview.model.AnimeRepo
import com.alecbrando.homeworkrecyclerview.model.models.Animes
import com.alecbrando.homeworkrecyclerview.util.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class AnimeViewModel: ViewModel() {
    private val repo = AnimeRepo

    private var _animeList: MutableLiveData<Resource> = MutableLiveData(Resource.Loading)
    val animeList: LiveData<Resource> get() = _animeList

    fun fetchAnime(animes: Animes) = viewModelScope.launch(Dispatchers.Main) {
        _animeList.value = Resource.Loading
        _animeList.value = repo.fetchAnimeList(animes)
    }
}