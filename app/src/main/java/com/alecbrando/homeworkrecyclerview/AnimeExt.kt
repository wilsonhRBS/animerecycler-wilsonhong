package com.alecbrando.homeworkrecyclerview

import android.widget.ImageView
import com.bumptech.glide.Glide

const val delay_time = 1000L

fun ImageView.loadImage(url: String) {
    Glide.with(context).load(url).into(this)
}