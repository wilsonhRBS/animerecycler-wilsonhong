package com.alecbrando.homeworkrecyclerview.views

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.alecbrando.homeworkrecyclerview.databinding.AnimeCardBinding
import com.alecbrando.homeworkrecyclerview.loadImage
import com.alecbrando.homeworkrecyclerview.model.models.Data

class ListAdapter: RecyclerView.Adapter<ListAdapter.ListViewHolder>() {
    private lateinit var animeList: List<Data>

    class ListViewHolder (
        private val binding: AnimeCardBinding
    ): RecyclerView.ViewHolder(binding.root) {
        fun apply(anime: Data) {
            binding.animeIv.loadImage(anime.attributes.coverImage.large)
            binding.animeTv.text = anime.attributes.canonicalTitle
            binding.root.setOnClickListener {
                val action = ListFragmentDirections.actionListFragmentToDetailFragment(anime)
                binding.root.findNavController().navigate(action)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListViewHolder {
        val binding = AnimeCardBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ListViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ListViewHolder, position: Int) {
        val anime = animeList[position]
        holder.apply(anime)
    }

    override fun getItemCount(): Int {
        return animeList.size
    }

    fun giveData(data: List<Data>) {
        animeList = data
    }
}