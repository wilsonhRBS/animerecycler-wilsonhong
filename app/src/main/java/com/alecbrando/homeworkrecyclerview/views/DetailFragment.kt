package com.alecbrando.homeworkrecyclerview.views

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import com.alecbrando.homeworkrecyclerview.databinding.FragmentDetailBinding
import com.alecbrando.homeworkrecyclerview.databinding.FragmentListBinding
import com.alecbrando.homeworkrecyclerview.loadImage

class DetailFragment: Fragment() {
    private val args by navArgs<DetailFragmentArgs>()
    private var _binding: FragmentDetailBinding? = null
    private val binding: FragmentDetailBinding get() = _binding!!

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentDetailBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
    }

    private fun initViews() = with(binding) {
        ivMain.loadImage(args.anime.attributes.coverImage.large)
        tvName.text = args.anime.attributes.canonicalTitle
        tvRating.text = args.anime.attributes.averageRating
        tvDescription.text = args.anime.attributes.synopsis
    }
}