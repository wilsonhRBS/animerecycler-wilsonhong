package com.alecbrando.homeworkrecyclerview.views

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.alecbrando.homeworkrecyclerview.databinding.FragmentListBinding
import com.alecbrando.homeworkrecyclerview.model.models.Animes
import com.alecbrando.homeworkrecyclerview.util.Resource
import com.alecbrando.homeworkrecyclerview.util.getJsonDataFromAsset
import com.alecbrando.homeworkrecyclerview.viewmodels.AnimeViewModel
import com.example.randomcolorrecycler.viewmodel.AnimeViewModelFactory
import com.google.gson.Gson

class ListFragment : Fragment() {
    private var _binding: FragmentListBinding? = null
    private val binding: FragmentListBinding get() = _binding!!
    private lateinit var animeViewModel: AnimeViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentListBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        animeViewModel = ViewModelProvider(this, AnimeViewModelFactory())[AnimeViewModel::class.java]
        val data = getJsonDataFromAsset(requireContext(), "anime.json")
        val actualData = Gson().fromJson(data, Animes::class.java)

        initViews(actualData)
    }

    private fun initViews(data: Animes) = with(binding) {
        animeViewModel.animeList.observe(viewLifecycleOwner) {
            when(it) {
                is Resource.Loading -> {
                    binding.progress.isVisible = true
                    binding.menuBtn1.isEnabled = false

                }
                is Resource.Success -> {
                    binding.progress.isVisible = false
                    binding.menuBtn1.isEnabled = true
                    binding.recycler.layoutManager = LinearLayoutManager(requireContext())
                    binding.recycler.adapter = ListAdapter().apply {
                        giveData(it.data)
                    }
                }
            }
        }

        menuBtn1.setOnClickListener {
            animeViewModel.fetchAnime(data)
        }
        animeViewModel.fetchAnime(data)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}